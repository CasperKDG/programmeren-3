package be.kdg.distrib.utils;

import java.lang.reflect.Field;

public class FactoryUtils {
    public static void setObjectField(Object object, Field field, String parameter) throws IllegalAccessException {
        switch (field.getType().getName()) {
            case "int":
                field.setInt(object, Integer.parseInt(parameter));
                break;
            case "double":
                field.setDouble(object, Double.parseDouble(parameter));
                break;
            case "boolean":
                field.setBoolean(object, Boolean.parseBoolean(parameter));
                break;
            case "char":
                field.setChar(object, parameter.charAt(0));
                break;
            case "java.lang.String":
                field.set(object, parameter);
                break;
        }
    }
}
