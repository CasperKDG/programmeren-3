package be.kdg.distrib.skeletonFactory;

import java.lang.reflect.Proxy;

/**
 * SkeletonFactory is used to create Skeletons.
 *
 * @author Casper Van Bogaert
 * @version 1.0
 */
public class SkeletonFactory {
    /**
     * Create a new Skeleton instance for the given Object.
     *
     * @param object the given Object to create a Skeleton for.
     * @return the new Skeleton.
     */
    public static Object createSkeleton(Object object) {
        return Proxy.newProxyInstance(
                Thread.currentThread().getContextClassLoader(),
                new Class[]{Skeleton.class},
                new SkeletonInvocationHandler(object)
        );
    }
}
