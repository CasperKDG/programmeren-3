package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * SkeletonInvocationHandler is a custom InvocationHandler used for generated Skeletons.
 *
 * @author Casper Van Bogaert
 * @version 1.0
 */
public class SkeletonInvocationHandler implements InvocationHandler {
    private final Object object;
    private final MessageManager messageManager;

    public SkeletonInvocationHandler(Object object) {
        this.object = object;
        this.messageManager = new MessageManager();
    }

    /**
     * Handles method calls for the Skeleton.
     *
     * @param proxy  the given proxy.
     * @param method the given Method that was called.
     * @param args   the given arguments used to call the method.
     * @return the required return type.
     * @throws Throwable any exception encountered.
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        switch (method.getName()) {
            case "getAddress":
                return messageManager.getMyAddress();
            case "handleRequest":
                if (args != null) {
                    SkeletonRequestHandler.handleRequest((MethodCallMessage) args[0], object, messageManager);
                } else {
                    throw new RuntimeException("Not enough arguments");
                }
                break;
            case "run":
                run();
                break;
        }
        return null;
    }

    /**
     * Start listening to messages with the given MessageManager.
     */
    private void run() {
        new Thread(new SkeletonRequestHandler(messageManager, object)).start();
    }
}
