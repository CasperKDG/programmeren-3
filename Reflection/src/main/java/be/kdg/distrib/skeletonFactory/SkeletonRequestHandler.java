package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static be.kdg.distrib.utils.FactoryUtils.setObjectField;

/**
 * SkeletonRequestHandler handles incoming requests for Skeletons.
 *
 * @author Casper Van Bogaert
 * @version 1.0
 */
@RequiredArgsConstructor
public class SkeletonRequestHandler implements Runnable {
    private final MessageManager messageManager;
    private final Object object;

    /**
     * Handles all incoming requests, calls the right method with the right arguments.
     * Both primitive arguments and custom Objects are supported as arguments.
     * Primitives are passed onto {@link #handleNormalParameter(List, Parameter, Map.Entry)}
     * Objects are passed to {@link #handleObject(MethodCallMessage, Method, List, List, String)}
     *
     * @param methodCallMessage the given incoming request message.
     * @param object            the given object for which a Skeleton was generated.
     * @param messageManager    the given MessageManager used to send replies.
     * @throws Throwable any exception encountered is thrown.
     */
    public static void handleRequest(MethodCallMessage methodCallMessage, Object object, MessageManager messageManager)
            throws Throwable {
        Method method = Arrays.stream(object.getClass().getDeclaredMethods())
                .filter(m -> m.getName().equals(methodCallMessage.getMethodName()))
                .findAny().orElseThrow(RuntimeException::new);

        List<Object> args = new ArrayList<>();
        List<String> handledObjects = new ArrayList<>();
        for (Parameter methodParameter : method.getParameters()) { //for each required arg
            for (Map.Entry<String, String> entry : methodCallMessage.getParameters().entrySet()) {//for each found arg
                String key = entry.getKey();
                if (key.contains(".")) {
                    handleObject(methodCallMessage, method, args, handledObjects, key);
                } else if (key.equals(methodParameter.getName())) {
                    handleNormalParameter(args, methodParameter, entry);
                }
            }
        }

        int parameterCount = handledObjects.size(); //amount of objects
        parameterCount += methodCallMessage.getParameters().entrySet().stream()
                .filter(p -> !p.getKey().contains(".")).count();//amount of non objects
        if (method.getParameters().length != parameterCount) {
            throw new RuntimeException("Parameter counts don't match");
        } else {
            Object returnValue = method.invoke(object, args.toArray());
            sendReply(methodCallMessage, messageManager, method, returnValue);
        }
    }

    /**
     * Handles multiple given MethodCallMessage parameters, turns them into the right Object, initialises its fields
     * using {@link be.kdg.distrib.utils.FactoryUtils#setObjectField(Object, Field, String)} and adds that object to the
     * given args List.
     *
     * @param methodCallMessage the given MethodCallMessaged used to get parameters from.
     * @param method            the given Method used to determine the required Type of Object needed for this parameter.
     * @param args              the given arguments List to add the created Object to.
     * @param handledObjects    the given List of Object names used to check whether this Object has already been
     *                          handled and added to the given arguments List.
     * @param key               the given key to find the Object's name in.
     * @throws Throwable any exception encountered is thrown.
     */
    private static void handleObject(MethodCallMessage methodCallMessage, Method method, List<Object> args,
                                     List<String> handledObjects, String key) throws Throwable {
        String parameterObjectName = key.split("\\.")[0];
        if (!handledObjects.contains(parameterObjectName)) {
            Object o = Arrays.stream(method.getParameters())
                    .filter(p -> isObject(p.getType().getName()))
                    .findFirst()
                    .orElseThrow(RuntimeException::new)
                    .getType().getDeclaredConstructor().newInstance();

            for (Field field : o.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                String fieldValue = methodCallMessage.getParameter(parameterObjectName + "." + field.getName());
                setObjectField(o, field, fieldValue);
            }
            handledObjects.add(parameterObjectName);
            args.add(o);
        }
    }

    /**
     * Handles a single parameter that's a primitive type or String by parsing it and adding it to the given args List.
     *
     * @param args                  the given List to add the parameter to.
     * @param methodParameter       the given Method's Parameter used to check what kind of Type is required.
     * @param foundMessageParameter the given found parameter that contains the value to parse.
     */
    private static void handleNormalParameter(List<Object> args, Parameter methodParameter,
                                              Map.Entry<String, String> foundMessageParameter) {
        switch (methodParameter.getType().getName()) {
            case "int":
                args.add(Integer.parseInt(foundMessageParameter.getValue()));
                break;
            case "double":
                args.add(Double.parseDouble(foundMessageParameter.getValue()));
                break;
            case "boolean":
                args.add(Boolean.parseBoolean(foundMessageParameter.getValue()));
                break;
            case "char":
                args.add(foundMessageParameter.getValue().charAt(0));
                break;
            default:
                args.add(methodParameter.getType().cast(foundMessageParameter.getValue()));
        }
    }

    /**
     * Check if the given Class name is a custom -unsupported- Object.
     *
     * @param name the given name to check.
     * @return true if the given name is a custom -unsupported- Object.
     */
    public static boolean isObject(String name) {
        List<String> supported = new ArrayList<>();
        supported.add(Boolean.class.getName());
        supported.add(boolean.class.getName());
        supported.add(Character.class.getName());
        supported.add(char.class.getName());
        supported.add(Integer.class.getName());
        supported.add(int.class.getName());
        supported.add(Double.class.getName());
        supported.add(double.class.getName());
        supported.add("java.lang.String");
        return !supported.contains(name);
    }

    /**
     * Uses the given MessageManager to send a reply.
     *
     * @param methodCallMessage the given MethodCallMessage to send.
     * @param messageManager    the given MEssageManager used to send the given MethodCallMessage.
     * @param method            the given Method used to check the returnType for.
     * @param returnValue       the given value received by invoking the given Method before this method is called.
     * @throws Throwable any exception encountered is thrown.
     */
    private static void sendReply(MethodCallMessage methodCallMessage, MessageManager messageManager, Method method,
                                  Object returnValue) throws Throwable {
        switch (method.getReturnType().getName()) {
            case "void":
                methodCallMessage.setParameter("result", "Ok");
                break;
            case "int":
            case "double":
            case "boolean":
            case "char":
            case "java.lang.String":
                methodCallMessage.setParameter("result", returnValue.toString());
                break;
            default:
                for (Field field : returnValue.getClass().getDeclaredFields()) {
                    field.setAccessible(true);
                    methodCallMessage.setParameter("result." + field.getName(), field.get(returnValue).toString());
                }
                break;
        }
        messageManager.send(methodCallMessage, methodCallMessage.getOriginator());
    }

    /**
     * Listen for incoming messages and pass them on to {@link #handleRequest(MethodCallMessage, Object, MessageManager)}.
     */
    @SuppressWarnings("InfiniteLoopStatement")
    @Override
    public void run() {
        while (true) {
            MethodCallMessage methodCallMessage = messageManager.wReceive();
            try {
                handleRequest(methodCallMessage, object, messageManager);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
    }
}
