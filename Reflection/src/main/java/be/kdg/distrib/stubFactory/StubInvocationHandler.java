package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import static be.kdg.distrib.utils.FactoryUtils.setObjectField;

/**
 * StubInvocationHandler is a custom InvocationHandler used for generated Stubs.
 *
 * @author Casper Van Bogaert
 * @version 1.0
 */
public class StubInvocationHandler implements InvocationHandler {
    private final NetworkAddress networkAddress;
    private final MessageManager messageManager;

    public StubInvocationHandler(NetworkAddress networkAddress) {
        this.networkAddress = networkAddress;
        messageManager = new MessageManager();
    }

    /**
     * Handles method calls for the Stub. Creates a message and sends it, then waits for a response and returns it in
     * the proper format.
     *
     * @param proxy  the given proxy.
     * @param method the given Method that was called.
     * @param args   the given arguments used to call the method.
     * @return the required return type.
     * @throws Throwable any exception encountered.
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), method.getName());
        setMessageParameters(args, message);
        messageManager.send(message, networkAddress);

        MethodCallMessage reply = messageManager.wReceive();
        String result = reply.getParameter("result");
        return handleResult(method, reply, result);
    }

    /**
     * Sets the Parameters for the given MethodCallMessage to the required values.
     *
     * @param args    the given Method Parameters that are required.
     * @param message the given MethodCallMessage to add Parameters to.
     * @throws Throwable any exception encountered.
     */
    private void setMessageParameters(Object[] args, MethodCallMessage message) throws Throwable {
        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                if (args[i].getClass().getDeclaredFields().length > 0 &&
                        !args[i].getClass().getPackage().toString().contains("java.lang")) {
                    for (Field field : args[i].getClass().getDeclaredFields()) {
                        field.setAccessible(true);
                        message.setParameter("arg" + i + "." + field.getName(), field.get(args[i]).toString());
                    }
                } else {
                    message.setParameter("arg" + i, args[i].toString());
                }
            }
        }
    }

    /**
     * Handles the result of a Method invocation by converting it to the right Type.
     * If the Method returned a custom -unsupported- Object, turns the result into the right Object, initialises its
     * fields using {@link be.kdg.distrib.utils.FactoryUtils#setObjectField(Object, Field, String)}.
     *
     * @param method the given Method to get the required returnType from.
     * @param reply  the reply used to get parameter values from.
     * @param result the result from the Skeleton call. null if the returned Type is a custom -unsupported- Object.
     * @return the right Object or supported Type.
     * @throws Throwable any exception encountered.
     */
    private Object handleResult(Method method, MethodCallMessage reply, String result) throws Throwable {
        if (result == null) {
            Object object = method.getReturnType().getDeclaredConstructor().newInstance();
            for (Field field : object.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                String parameter = reply.getParameters().entrySet().stream()
                        .filter(p -> p.getKey().contains(field.getName()))
                        .findFirst()
                        .orElseThrow(RuntimeException::new)
                        .getValue();

                setObjectField(object, field, parameter);
            }
            return object;
        } else {
            switch (method.getReturnType().getName()) {
                case "int":
                    return Integer.parseInt(result);
                case "double":
                    return Double.parseDouble(result);
                case "boolean":
                    return Boolean.parseBoolean(result);
                case "char":
                    return result.charAt(0);
                case "java.lang.String":
                    return result;
            }
        }
        return null;
    }
}
