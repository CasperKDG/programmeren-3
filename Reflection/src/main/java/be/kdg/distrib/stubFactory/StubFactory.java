package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * StubFactory is used to create Stubs.
 *
 * @author Casper Van Bogaert
 * @version 1.0
 */
public class StubFactory {
    /**
     * Create a new Stub instance for the given Object.
     *
     * @param c    the given Class to create a Stub for.
     * @param ip   the given ip used to create a NetworkAddress instance.
     * @param port the given port number used to create a NetworkAddress instance.
     * @return the new Skeleton.
     */
    public static Object createStub(Class<?> c, String ip, int port) {
        NetworkAddress networkAddress = new NetworkAddress(ip, port);
        InvocationHandler handler = new StubInvocationHandler(networkAddress);
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{c}, handler);
    }
}
