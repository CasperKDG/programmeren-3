package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;
import be.kdg.examen.gedistribueerde.server.Server;

/**
 * ServerStub sends calls for a Server to a Skeleton.
 *
 * @author Casper Van Bogaert
 * @version 1.0
 */
public class ServerStub implements Server {
    private final NetworkAddress serverAddress;
    private final NetworkAddress documentAddress;
    private final MessageManager messageManager;

    public ServerStub(NetworkAddress serverAddress, NetworkAddress documentAddress) {
        this.serverAddress = serverAddress;
        this.documentAddress = documentAddress;
        this.messageManager = new MessageManager();
    }

    /**
     * Send a MethodCallMessage for a log method call to the Skeleton at this ServerStub's serverAddress.
     *
     * @param document the given Document to log.
     */
    @Override
    public void log(Document document) {
        MethodCallMessage message = initMessage("log");
        message.setParameter("text", document.getText());
        messageManager.send(message, serverAddress);
        checkEmptyReply();
    }

    /**
     * Send a MethodCallMessage for a create method call to the Skeleton at this ServerStub's serverAddress, then gets
     * the new Document from the reply.
     *
     * @param s the given String used to create a Document.
     * @return the created Document.
     */
    @Override
    public Document create(String s) {
        MethodCallMessage request = initMessage("create");
        request.setParameter("text", s);
        messageManager.send(request, serverAddress);

        Document document = null;
        while (document == null) {
            MethodCallMessage response = messageManager.wReceive();
            if (!response.getMethodName().equals("createResponse")) {
                continue;
            }
            document = new DocumentImpl(response.getParameter("text"));
        }

        return document;
    }

    /**
     * Send a MethodCallMessage for a toUpper method call to the Skeleton at this ServerStub's serverAddress.
     *
     * @param document the given Document to toUpper.
     */
    @Override
    public void toUpper(Document document) {
        MethodCallMessage request = initMessage("toUpper");
        messageManager.send(request, serverAddress);
        checkEmptyReply();
    }

    /**
     * Send a MethodCallMessage for a toLower method call to the Skeleton at this ServerStub's serverAddress.
     *
     * @param document the given Document to toLower.
     */
    @Override
    public void toLower(Document document) {
        MethodCallMessage request = initMessage("toLower");
        messageManager.send(request, serverAddress);
        checkEmptyReply();
    }

    /**
     * Send a MethodCallMessage for a type method call to the Skeleton at this ServerStub's serverAddress.
     *
     * @param document the given Document to type.
     * @param text     the given text to type.
     */
    @Override
    public void type(Document document, String text) {
        MethodCallMessage request = initMessage("type");
        request.setParameter("text", text);
        messageManager.send(request, serverAddress);
        checkEmptyReply();
    }

    /**
     * Creates a MethodCallMessage with an ip and port used to send to the Skeleton at this ServerStub's serverAddress.
     *
     * @param method the method to call.
     * @return the created MethodCallMessage.
     */
    private MethodCallMessage initMessage(String method) {
        MethodCallMessage request = new MethodCallMessage(messageManager.getMyAddress(), method);
        request.setParameter("ip", documentAddress.getIpAddress());
        request.setParameter("port", String.valueOf(documentAddress.getPortNumber()));
        return request;
    }

    /**
     * Check if an MethodCallMessage with a Parameter 'result' with value 'Ok' is received.
     */
    private void checkEmptyReply() {
        String value = "";
        while (!"Ok".equals(value)) {
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            value = reply.getParameter("result");
        }
    }
}
