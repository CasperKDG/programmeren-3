package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

/**
 * DocumentSkeleton handles incoming calls for its document
 *
 * @author Casper Van Bogaert
 * @version 1.0
 */
public class DocumentSkeleton implements Runnable {
    private final DocumentImpl document;
    private final MessageManager messageManager;

    public DocumentSkeleton(DocumentImpl document) {
        this.document = document;
        this.messageManager = new MessageManager();
    }

    /**
     * @return this DocumentSkeleton's NetworkAddress.
     */
    public NetworkAddress getAddress() {
        return messageManager.getMyAddress();
    }

    /**
     * Start listening for incoming messages. Passes any incoming message to {@link #handleMessage(MethodCallMessage)}.
     */
    @SuppressWarnings("InfiniteLoopStatement")
    @Override
    public void run() {
        while (true) {
            MethodCallMessage message = messageManager.wReceive();
            handleMessage(message);
        }
    }

    /**
     * Takes a given MethodCallMessage and uses it to perform actions on this DocumentSkeleton's document.
     * Sends replies using this DocumentSkeleton's MessageManager.
     *
     * @param message the given MethodCallMessage used to pass the methodname and parameters.
     */
    private void handleMessage(MethodCallMessage message) {
        String method = message.getMethodName();
        switch (method) {
            case "getText": {
                MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "getTextResponse");
                reply.setParameter("text", document.getText());
                messageManager.send(reply, message.getOriginator());
            }
            break;
            case "setText": {
                String text = message.getParameter("text");
                document.setText(text);
                sendEmptyReply(message);
            }
            break;
            case "append": {
                char c = message.getParameter("char").charAt(0);
                document.append(c);
                sendEmptyReply(message);
            }
            break;
            case "setChar": {
                int position = Integer.parseInt(message.getParameter("position"));
                char c = message.getParameter("char").charAt(0);
                document.setChar(position, c);
                sendEmptyReply(message);
            }
            break;
            default:
                break;
        }
    }

    /**
     * Sends an empty reply to the given MethodCallMessage's originator.
     *
     * @param request the given MethodCallMessage used for an originator NetworkAddress.
     */
    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }
}
