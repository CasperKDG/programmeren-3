package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.NetworkAddress;
import be.kdg.examen.gedistribueerde.server.Server;

public class StartNonDistributed {
    public static void main(String[] args) {
        String serverIp = "192.168.0.29";
        int serverPort = 51182;

        NetworkAddress serverAddress = new NetworkAddress(serverIp, serverPort);

        DocumentImpl document = new DocumentImpl();
        DocumentSkeleton documentSkeleton = new DocumentSkeleton(document);
        new Thread(documentSkeleton).start();

        Server serverStub = new ServerStub(serverAddress, documentSkeleton.getAddress());
        Client client = new Client(serverStub, document);
        client.run();
    }
}
