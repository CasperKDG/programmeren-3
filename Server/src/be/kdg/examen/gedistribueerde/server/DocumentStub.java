package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.Document;
import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

/**
 * DocumentStub sends calls for a Document to a Skeleton.
 *
 * @author Casper Van Bogaert
 * @version 1.0
 */
public class DocumentStub implements Document {
    private final MessageManager messageManager;
    private final NetworkAddress documentAddress;//address of client

    public DocumentStub(NetworkAddress networkAddress) {
        this.messageManager = new MessageManager();
        this.documentAddress = networkAddress;
    }

    /**
     * Check if an MethodCallMessage with a Parameter 'result' with value 'Ok' is received.
     */
    private void checkEmptyReply() {
        String value = "";
        while (!"Ok".equals(value)) {
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            value = reply.getParameter("result");
        }
    }

    /**
     * Send a MethodCallMessage for a getText method call to the Skeleton at this DocumentStub's documentAddress.
     *
     * @return the Skeleton's Document's text.
     */
    @Override
    public String getText() {
        MethodCallMessage request = new MethodCallMessage(messageManager.getMyAddress(), "getText");

        messageManager.send(request, documentAddress);

        String text = "";
        while (text.equals("")) {
            MethodCallMessage response = messageManager.wReceive();
            if (!response.getMethodName().equals("getTextResponse")) {
                continue;
            }
            text = response.getParameter("text");
        }
        return text;
    }

    /**
     * Send a MethodCallMessage for a setText method call to the Skeleton at this DocumentStub's documentAddress.
     *
     * @param text the given text to set.
     */
    @Override
    public void setText(String text) {
        MethodCallMessage request = new MethodCallMessage(messageManager.getMyAddress(), "setText");
        request.setParameter("text", text);
        messageManager.send(request, documentAddress);
        checkEmptyReply();
    }

    /**
     * Send a MethodCallMessage for an append method call to the Skeleton at this DocumentStub's documentAddress.
     *
     * @param c the given char to append.
     */
    @Override
    public void append(char c) {
        MethodCallMessage request = new MethodCallMessage(messageManager.getMyAddress(), "append");
        request.setParameter("char", String.valueOf(c));
        messageManager.send(request, documentAddress);
        this.checkEmptyReply();
    }

    /**
     * Send a MethodCallMessage for a setChar method call to the Skeleton at this DocumentStub's documentAddress.
     *
     * @param position the given position to set the given char at.
     * @param c        the given char to set.
     */
    @Override
    public void setChar(int position, char c) {
        MethodCallMessage request = new MethodCallMessage(messageManager.getMyAddress(), "setChar");
        request.setParameter("position", String.valueOf(position));
        request.setParameter("char", String.valueOf(c));
        messageManager.send(request, documentAddress);
        this.checkEmptyReply();
    }
}
