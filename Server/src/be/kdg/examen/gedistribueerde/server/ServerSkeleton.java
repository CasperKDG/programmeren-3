package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.Document;
import be.kdg.examen.gedistribueerde.client.DocumentImpl;
import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

/**
 * ServerSkeleton handles incoming calls for its Server.
 *
 * @author Casper Van Bogaert
 * @version 1.0
 */
public class ServerSkeleton {
    private final Server server;
    private final MessageManager messageManager;

    /**
     * Creates a new ServerSkeleton instance with a given Server.
     *
     * @param server the given server used to instantiate this ServerSkeleton.
     */
    public ServerSkeleton(Server server) {
        this.server = server;
        this.messageManager = new MessageManager();
    }

    /**
     * @return this ServerSkeleton's NetworkAddress.
     */
    public NetworkAddress getAddress() {
        return messageManager.getMyAddress();
    }

    /**
     * Starts listening to messages and passes them on to the handleMethodCall Method
     */
    @SuppressWarnings("InfiniteLoopStatement")
    public void run() {
        while (true) {
            MethodCallMessage methodCallMessage = messageManager.wReceive();
            handleRequest(methodCallMessage);
        }
    }

    /**
     * Handles incoming MethodCallMessages.
     *
     * @param message the given MethodCallMessage to handle.
     */
    private void handleRequest(MethodCallMessage message) {
        String ip = message.getParameter("ip");
        int port = Integer.parseInt(message.getParameter("port"));
        NetworkAddress address = new NetworkAddress(ip, port);

        String methodName = message.getMethodName();
        switch (methodName) {
            case "log":
                handleLog(message);
                break;
            case "create":
                handleCreate(message);
                break;
            case "toUpper":
                handleToUpper(message, address);
                break;
            case "toLower":
                handleToLower(message, address);
                break;
            case "type":
                handleType(message, address);
                break;
        }
    }

    /**
     * Gets the 'text' Parameter from the given MethodCallMessage and uses it to create a new DocumentImpl.
     * Then calls log on this ServerSkeleton's Server with the created Document.
     *
     * @param message the given MethodCallMessage used to get its originator NetworkAddress and text.
     */
    private void handleLog(MethodCallMessage message) {
        Document document = new DocumentImpl(message.getParameter("text"));
        server.log(document);
        sendEmptyReply(message);
    }

    /**
     * Gets the 'text' Parameter from the given MethodCallMessage.
     * Then calls create on this ServerSkeleton's Server with the text and sends the result back.
     *
     * @param message the given MethodCallMessage used to get its originator NetworkAddress and text.
     */
    private void handleCreate(MethodCallMessage message) {
        String text = message.getParameter("text");
        Document document = server.create(text);

        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "createResponse");
        reply.setParameter("text", document.getText());
        messageManager.send(reply, message.getOriginator());
    }

    /**
     * Creates a DocumentStub with the given NetworkAddress.
     * Calls toUpper on this ServerSkeleton's Server, with the new DocumentStub.
     *
     * @param message the given MethodCallMessage used to get its originator NetworkAddress.
     * @param address the given NetworkAddress used to create a DocumentStub.
     */
    private void handleToUpper(MethodCallMessage message, NetworkAddress address) {
        Document document = new DocumentStub(address);
        server.toUpper(document);
        sendEmptyReply(message);
    }

    /**
     * Creates a DocumentStub with the given NetworkAddress.
     * Calls toLower on this ServerSkeleton's Server, with the new DocumentStub.
     *
     * @param message the given MethodCallMessage used to get its originator NetworkAddress.
     * @param address the given NetworkAddress used to create a DocumentStub.
     */
    private void handleToLower(MethodCallMessage message, NetworkAddress address) {
        Document document = new DocumentStub(address);
        server.toLower(document);
        sendEmptyReply(message);
    }

    /**
     * Creates a DocumentStub with the given NetworkAddress.
     * Calls type on this ServerSkeleton's Server, with the new DocumentStub.
     *
     * @param message the given MethodCallMessage used to get its originator NetworkAddress and text.
     * @param address the given NetworkAddress used to create a DocumentStub.
     */
    private void handleType(MethodCallMessage message, NetworkAddress address) {
        Document document = new DocumentStub(address);
        String text = message.getParameter("text");
        server.type(document, text);
        sendEmptyReply(message);
    }

    /**
     * Sends an empty reply to the given MethodCallMessage's originator.
     *
     * @param request the given MethodCallMessage used for an originator NetworkAddress.
     */
    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }
}
