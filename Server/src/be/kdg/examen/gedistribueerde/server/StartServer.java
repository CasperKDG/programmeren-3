package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public class StartServer {
    public static void main(String[] args) {
        Server server = new ServerImpl();
        ServerSkeleton serverSkeleton = new ServerSkeleton(server);
        NetworkAddress address = serverSkeleton.getAddress();
        System.out.println("Address: " + address);
        serverSkeleton.run();
    }
}
